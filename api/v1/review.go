package v1

import (
	"context"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/book_market/api_gateway/api/models"
	pb "gitlab.com/book_market/api_gateway/genproto/book_service"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// @Security ApiKeyAuth
// @Router /reviews [post]
// @Summary Create a review
// @Description Create a review
// @Tags review
// @Accept json
// @Produce json
// @Param review body models.CreateReviewRequest true "Review"
// @Success 201 {object} models.Review
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) CreateReview(c *gin.Context) {
	var (
		req models.CreateReviewRequest
	)
	err := c.ShouldBindJSON(&req)
	if err != nil {
		h.logger.WithError(err).Error("failed to bind json")
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	result, err := h.grpcClient.ReviewService().Create(context.Background(), &pb.Review{
		BookId: req.BookId,
		CustomerId: req.CustomerId,
		Review: req.Review,
	})
	if err != nil {
		h.logger.WithError(err).Error("failed to create review")
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusCreated, parseReviewModel(result))
}

func parseReviewModel(review *pb.Review) *models.Review {
	return &models.Review{
		Id:        review.Id,
		BookId:    review.BookId,
		BookData: models.GetBookInfo{
			Name:          review.Book.Name,
			CategoryId:    review.Book.CategoryId,
			AuthorId:      review.Book.AuthorId,
			Description:   review.Book.Description,
			PublisherName: review.Book.PublisherName,
			Language:      review.Book.Language,
			Inscription:   review.Book.Inscription,
			Isbn:          review.Book.Isbn,
			Page:          review.Book.Page,
			Wrapper:       review.Book.Wrapper,
			Format:        review.Book.Format,
			ImageUrl:      review.Book.ImageUrl,
			Price:         float64(review.Book.Price),
			CreatedAt:     review.Book.CreatedAt,
		},
		CustomerId:    review.CustomerId,
		Review:    review.Review,
		CreatedAt: review.CreatedAt,
	}
}

// @Router /reviews/{id} [get]
// @Summary Get review by id
// @Description Get review by id
// @Tags review
// @Accept json
// @Produce json
// @Param id path int true "Id"
// @Success 201 {object} models.Review
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) GetReview(c *gin.Context) {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		h.logger.WithError(err).Error("failed to should bind json")
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	result, err := h.grpcClient.ReviewService().Get(context.Background(), &pb.ReviewIdRequest{Id: int64(id)})
	if err != nil {
		h.logger.WithError(err).Error("failed to get review")
		if s, _ := status.FromError(err); s.Code() == codes.NotFound {
			c.JSON(http.StatusNotFound, errorResponse(err))
		}
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusCreated, parseReviewModel(result))
}

// @Security ApiKeyAuth
// @Router /reviews/{id} [delete]
// @Summary Delete review
// @Description Delete review
// @Tags review
// @Accept json
// @Produce json
// @Param id path int true "ID"
// @Success 200 {object} models.ResponseOk
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) DeleteReview(c *gin.Context) {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		h.logger.WithError(err).Error("failed to bind json")
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	_, err = h.grpcClient.ReviewService().Delete(context.Background(), &pb.ReviewIdRequest{
		Id: int64(id),
	})
	if err != nil {
		h.logger.WithError(err).Error("failed to delete review")
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}
	c.JSON(http.StatusOK, models.ResponseOk{
		Message: "success",
	})
}

// @Router /reviews [get]
// @Summary Get all reviews
// @Description Get all reviews
// @Tags review
// @Accept json
// @Produce json
// @Param filter query models.GetAllParams false "Filter"
// @Success 200 {object} models.GetAllReviewsResponse
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) GetAllReviews(c *gin.Context) {
	req, err := validateGetAllParams(c)
	if err != nil {
		h.logger.WithError(err).Error("failed to bind json")
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	res, err := h.grpcClient.ReviewService().GetAll(context.Background(), &pb.GetAllReviewsRequest{
		Limit:  req.Limit,
		Page:   req.Page,
		Search: req.Search,
	})
	if err != nil {
		h.logger.WithError(err).Error("failed to get all reviews")
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	response := models.GetAllReviewsResponse{
		Reviews: make([]*models.Review, 0),
		Count:   int64(res.Count),
	}

	for _, review := range res.Reviews {
		response.Reviews = append(response.Reviews, parseReviewModel(review))
	}

	c.JSON(http.StatusOK, res)
}
