package v1

import (
	"context"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/book_market/api_gateway/api/models"
	pb "gitlab.com/book_market/api_gateway/genproto/book_service"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// @Security ApiKeyAuth
// @Router /authors [post]
// @Summary Create a author
// @Description Create a author
// @Tags author
// @Accept json
// @Produce json
// @Param author body models.CreateAuthorRequest true "Author"
// @Success 201 {object} models.Author
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) CreateAuthor(c *gin.Context) {
	var (
		req models.CreateAuthorRequest
	)
	err := c.ShouldBindJSON(&req)
	if err != nil {
		h.logger.WithError(err).Error("failed to bind json")
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	result, err := h.grpcClient.AuthorService().Create(context.Background(), &pb.Author{
		FullName: req.Fullname,
	})
	if err != nil {
		h.logger.WithError(err).Error("failed to create author")
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusCreated, parseAuthorModel(result))
}

func parseAuthorModel(author *pb.Author) *models.Author {
	return &models.Author{
		Id:        author.Id,
		Fullname:  author.FullName,
		CreatedAt: author.CreatedAt,
	}
}

// @Router /authors/{id} [get]
// @Summary Get author by id
// @Description Get author by id
// @Tags author
// @Accept json
// @Produce json
// @Param id path int true "Id"
// @Success 201 {object} models.Author
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) GetAuthor(c *gin.Context) {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		h.logger.WithError(err).Error("failed to should bind json")
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	result, err := h.grpcClient.AuthorService().Get(context.Background(), &pb.AuthorIdRequest{Id: int64(id)})
	if err != nil {
		h.logger.WithError(err).Error("failed to get author")
		if s, _ := status.FromError(err); s.Code() == codes.NotFound {
			c.JSON(http.StatusNotFound, errorResponse(err))
		}
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusCreated, parseAuthorModel(result))
}

// @Security ApiKeyAuth
// @Router /authors/{id} [delete]
// @Summary Delete author
// @Description Delete author
// @Tags author
// @Accept json
// @Produce json
// @Param id path int true "ID"
// @Success 200 {object} models.ResponseOk
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) DeleteAuthor(c *gin.Context) {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		h.logger.WithError(err).Error("failed to bind json")
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	_, err = h.grpcClient.AuthorService().Delete(context.Background(), &pb.AuthorIdRequest{
		Id: int64(id),
	})
	if err != nil {
		h.logger.WithError(err).Error("failed to delete author")
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}
	c.JSON(http.StatusOK, models.ResponseOk{
		Message: "success",
	})
}

// @Router /authors [get]
// @Summary Get all authors
// @Description Get all authors
// @Tags author
// @Accept json
// @Produce json
// @Param filter query models.GetAllParams false "Filter"
// @Success 200 {object} models.GetAllAuthorsResponse
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) GetAllAuthors(c *gin.Context) {
	req, err := validateGetAllParams(c)
	if err != nil {
		h.logger.WithError(err).Error("failed to bind json")
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	res, err := h.grpcClient.AuthorService().GetAll(context.Background(), &pb.GetAllAuthorsRequest{
		Limit: req.Limit,
		Page: req.Page,
		Search: req.Search,
	})
	if err != nil {
		h.logger.WithError(err).Error("failed to get all authors")
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusCreated, getAuthorsResponse(res))
}

func getAuthorsResponse(data *pb.GetAllAuthorsResponse) *models.GetAllAuthorsResponse {
	response := models.GetAllAuthorsResponse{
		Authors: make([]*models.Author, 0),
		Count:int64(data.Count),
	}

	for _, author := range data.Authors {
		response.Authors = append(response.Authors, parseAuthorModel(author))
	}

	return &response
}