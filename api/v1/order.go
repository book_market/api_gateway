package v1

import (
	"context"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/book_market/api_gateway/api/models"
	pb "gitlab.com/book_market/api_gateway/genproto/order_service"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// @Router /orders [post]
// @Summary Create a order
// @Description Create a order
// @Tags order
// @Accept json
// @Produce json
// @Param category body models.CreateOrderRequest true "Order"
// @Success 201 {object} models.Order
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) CreateOrder(c *gin.Context) {
	var (
		req models.CreateOrderRequest
	)
	err := c.ShouldBindJSON(&req)
	if err != nil {
		h.logger.WithError(err).Error("failed to bind json")
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	result, err := h.grpcClient.OrderService().Create(context.Background(), &pb.Order{
		UserId:        req.UserId,
		OrderNumber:   req.OrderNumber,
		TotalAmount:   int64(req.TotalAmount),
		Region:        req.Region,
		District:      req.District,
		Address:       req.Address,
		Status:        req.Status,
		PaymentStatus: req.PaymentStatus,
	})
	if err != nil {
		h.logger.WithError(err).Error("failed to create order")
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusCreated, parseOrderModel(result))
}

func parseOrderModel(order *pb.Order) *models.Order {
	return &models.Order{
		Id:            order.Id,
		UserId:        order.UserId,
		OrderNumber:   order.OrderNumber,
		TotalAmount:   float64(order.TotalAmount),
		Region:        order.Region,
		District:      order.District,
		Address:       order.Address,
		Status:        order.Status,
		PaymentStatus: order.PaymentStatus,
		CreatedAt:     order.CreatedAt,
	}
}

// @Router /orders/{id} [get]
// @Summary Get order by id
// @Description Get order by id
// @Tags order
// @Accept json
// @Produce json
// @Param id path int true "Id"
// @Success 201 {object} models.Order
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) GetOrder(c *gin.Context) {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		h.logger.WithError(err).Error("failed to should bind json")
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	result, err := h.grpcClient.OrderService().Get(context.Background(), &pb.OrderIdRequest{Id: int64(id)})
	if err != nil {
		h.logger.WithError(err).Error("failed to get order")
		if s, _ := status.FromError(err); s.Code() == codes.NotFound {
			c.JSON(http.StatusNotFound, errorResponse(err))
		}
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusCreated, parseOrderModel(result))
}

// @Security ApiKeyAuth
// @Router /orders/{id} [delete]
// @Summary Delete order
// @Description Delete order
// @Tags order
// @Accept json
// @Produce json
// @Param id path int true "ID"
// @Success 200 {object} models.ResponseOk
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) DeleteOrder(c *gin.Context) {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		h.logger.WithError(err).Error("failed to bind json")
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	_, err = h.grpcClient.OrderService().Delete(context.Background(), &pb.OrderIdRequest{
		Id: int64(id),
	})
	if err != nil {
		h.logger.WithError(err).Error("failed to delete order")
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}
	c.JSON(http.StatusOK, models.ResponseOk{
		Message: "success",
	})
}

// @Router /orders [get]
// @Summary Get all orders
// @Description Get all orders
// @Tags order
// @Accept json
// @Produce json
// @Param filter query models.GetAllParams false "Filter"
// @Success 200 {object} models.GetAllOrdersResponse
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) GetAllOrders(c *gin.Context) {
	req, err := validateGetAllParams(c)
	if err != nil {
		h.logger.WithError(err).Error("failed to bind json")
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	res, err := h.grpcClient.OrderService().GetAll(context.Background(), &pb.GetAllOrdersRequest{
		Limit:  req.Limit,
		Page:   req.Page,
		Search: req.Search,
	})
	if err != nil {
		h.logger.WithError(err).Error("failed to get all orders")
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusCreated, getOrdersResponse(res))
}

func getOrdersResponse(data *pb.GetAllOrdersResponse) *models.GetAllOrdersResponse {
	response := models.GetAllOrdersResponse{
		Orders: make([]*models.Order, 0),
		Count:  data.Count,
	}

	for _, order := range data.Orders {
		response.Orders = append(response.Orders, parseOrderModel(order))
	}

	return &response
}

// @Security ApiKeyAuth
// @Router /orders [put]
// @Summary Update a status order
// @Description Update a status order
// @Tags order
// @Accept json
// @Produce json
// @Param order body models.UpdateStatus true "Order"
// @Success 201 {object} models.Order
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) UpdateStatus(c *gin.Context) {
	var (
		req models.UpdateStatus
	)
	err := c.ShouldBindJSON(&req)
	if err != nil {
		h.logger.WithError(err).Error("failed to bind json")
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	result, err := h.grpcClient.OrderService().Update(context.Background(), &pb.Order{
		Status: req.Status,
	})
	if err != nil {
		h.logger.WithError(err).Error("failed to update status order")
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusCreated, parseOrderModel(result))
}
