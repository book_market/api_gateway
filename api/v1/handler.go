package v1

import (
	"errors"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"gitlab.com/book_market/api_gateway/api/models"
	"gitlab.com/book_market/api_gateway/config"
	grpcPkg "gitlab.com/book_market/api_gateway/pkg/grpc_client"
)

var (
	ErrWrongEmailOrPass = errors.New("wrong email or password")
	ErrEmailExists      = errors.New("email already exists")
	ErrUserNotVerified  = errors.New("user not verified")
	ErrIncorrectCode    = errors.New("incorrect verification code")
	ErrCodeExpired      = errors.New("verification code has been expired")
	ErrNotAllowed       = errors.New("method not allowed")
	ErrWeakPassword     = errors.New("password must contain at least one small letter, one capital letter, one number, one symbol")
)

type handlerV1 struct {
	cfg        *config.Config
	grpcClient grpcPkg.GrpcClientI
	logger     *logrus.Logger
}

type HandlerV1Options struct {
	Cfg        *config.Config
	GrpcClient grpcPkg.GrpcClientI
	Logger     *logrus.Logger
}

func New(options *HandlerV1Options) *handlerV1 {
	return &handlerV1{
		cfg:        options.Cfg,
		grpcClient: options.GrpcClient,
		logger:     options.Logger,
	}
}

func errorResponse(err error) *models.ErrorResponse {
	return &models.ErrorResponse{
		Error: err.Error(),
	}
}

func validateGetAllBooksParams(c *gin.Context) (*models.GetAllBooksParams, error) {
	var (
		limit       int = 10
		page        int = 1
		author_id   int
		category_id int
		price_from  float64
		price_to    float64
		err         error
	)

	if c.Query("limit") != "" {
		limit, err = strconv.Atoi(c.Query("limit"))
		if err != nil {
			return nil, err
		}
	}

	if c.Query("page") != "" {
		page, err = strconv.Atoi(c.Query("page"))
		if err != nil {
			return nil, err
		}
	}

	if c.Query("author_id") != "" {
		author_id, err = strconv.Atoi(c.Query("author_id"))
		if err != nil {
			return nil, err
		}
	}

	if c.Query("category_id") != "" {
		category_id, err = strconv.Atoi(c.Query("category_id"))
		if err != nil {
			return nil, err
		}
	}

	if c.Query("price_from") != "" {
		price_from, err = strconv.ParseFloat(c.Query("price_from"), 64)
		if err != nil {
			return nil, err
		}
	}

	if c.Query("price_to") != "" {
		price_from, err = strconv.ParseFloat(c.Query("price_to"), 64)
		if err != nil {
			return nil, err
		}
	}

	return &models.GetAllBooksParams{
		Limit:         int32(limit),
		Page:          int32(page),
		Name:          c.Query("name"),
		AuthorId:      int64(author_id),
		CategoryId:    int64(category_id),
		PublisherName: c.Query("publisher_name"),
		Language:      c.Query("language"),
		Inscription:   c.Query("inscription"),
		Wrapper:       c.Query("wrapper"),
		PriceFrom:     price_from,
		PriceTo:       price_to,
	}, nil
}

func validateGetAllParams(c *gin.Context) (*models.GetAllParams, error) {
	var (
		limit int = 10
		page  int = 1
		err   error
	)

	if c.Query("limit") != "" {
		limit, err = strconv.Atoi(c.Query("limit"))
		if err != nil {
			return nil, err
		}
	}

	if c.Query("page") != "" {
		page, err = strconv.Atoi(c.Query("page"))
		if err != nil {
			return nil, err
		}
	}

	return &models.GetAllParams{
		Limit:  int32(limit),
		Page:   int32(page),
		Search: c.Query("search"),
	}, nil
}
