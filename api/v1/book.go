package v1

import (
	"context"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/book_market/api_gateway/api/models"
	pb "gitlab.com/book_market/api_gateway/genproto/book_service"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// @Security ApiKeyAuth
// @Router /books [post]
// @Summary Create a book
// @Description Create a book
// @Tags book
// @Accept json
// @Produce json
// @Param book body models.CreateBookRequest true "Book"
// @Success 201 {object} models.Book
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) CreateBook(c *gin.Context) {
	var (
		req models.CreateBookRequest
	)
	err := c.ShouldBindJSON(&req)
	if err != nil {
		h.logger.WithError(err).Error("failed to bind json")
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	result, err := h.grpcClient.BookService().Create(context.Background(), &pb.Book{
		Name:          req.Name,
		CategoryId:    req.CategoryId,
		AuthorId:      req.AuthorId,
		Description:   req.Description,
		PublisherName: req.PublisherName,
		Language:      req.Language,
		Inscription:   req.Inscription,
		Page:          req.Page,
		Isbn:          req.Isbn,
		Format:        req.Format,
		Wrapper:       req.Wrapper,
		Price:         float32(req.Price),
		ImageUrl:      req.ImageUrl,
	})
	if err != nil {
		h.logger.WithError(err).Error("failed to create book")
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusCreated, parseBookModel(result))
}

func parseBookModel(book *pb.Book) models.Book {
	return models.Book{
		Id:         book.Id,
		Name:       book.Name,
		CategoryId: book.CategoryId,
		CategoryData: models.GetCategoryInfo{
			Name:      book.Category.Name,
			ImageUrl:  book.Category.ImageUrl,
			CreatedAt: book.Category.CreatedAt,
		},
		AuthorId: book.AuthorId,
		AuthorData: models.GetAuthorInfo{
			Fullname:  book.Author.FullName,
			CreatedAt: book.Author.CreatedAt,
		},
		Description:   book.Description,
		PublisherName: book.PublisherName,
		Language:      book.Language,
		Inscription:   book.Inscription,
		Page:          book.Page,
		Isbn:          book.Isbn,
		Format:        book.Format,
		Wrapper:       book.Wrapper,
		Price:         float64(book.Price),
		ImageUrl:      book.ImageUrl,
		CreatedAt:     book.CreatedAt,
	}
}

// @Router /books/{id} [get]
// @Summary Get book by id
// @Description Get book by id
// @Tags book
// @Accept json
// @Produce json
// @Param id path int true "Id"
// @Success 201 {object} models.Book
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) GetBook(c *gin.Context) {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		h.logger.WithError(err).Error("failed to should bind json")
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	result, err := h.grpcClient.BookService().Get(context.Background(), &pb.BookIdRequest{Id: int64(id)})
	if err != nil {
		h.logger.WithError(err).Error("failed to get book")
		if s, _ := status.FromError(err); s.Code() == codes.NotFound {
			c.JSON(http.StatusNotFound, errorResponse(err))
		}
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusCreated, parseBookModel(result))
}

// @Security ApiKeyAuth
// @Router /books/{id} [delete]
// @Summary Delete book
// @Description Delete book
// @Tags book
// @Accept json
// @Produce json
// @Param id path int true "ID"
// @Success 200 {object} models.ResponseOk
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) DeleteBook(c *gin.Context) {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		h.logger.WithError(err).Error("failed to bind json")
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	_, err = h.grpcClient.BookService().Delete(context.Background(), &pb.BookIdRequest{
		Id: int64(id),
	})
	if err != nil {
		h.logger.WithError(err).Error("failed to delete book")
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}
	c.JSON(http.StatusOK, models.ResponseOk{
		Message: "success",
	})
}

// @Router /books [get]
// @Summary Get all books
// @Description Get all books
// @Tags book
// @Accept json
// @Produce json
// @Param filter query models.GetAllBooksParams false "Filter"
// @Success 200 {object} models.GetAllBooksResponse
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) GetAllBooks(c *gin.Context) {
	req, err := validateGetAllBooksParams(c)
	if err != nil {
		h.logger.WithError(err).Error("failed to bind json")
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	res, err := h.grpcClient.BookService().GetAll(context.Background(), &pb.GetAllBooksRequest{
		Limit:         req.Limit,
		Page:          req.Page,
		Name:          req.Name,
		AuthorId:      int32(req.AuthorId),
		CategoryId:    int32(req.CategoryId),
		PublisherName: req.PublisherName,
		Language:      req.Language,
		Inscription:   req.Inscription,
		Wrapper:       req.Wrapper,
		PriceFrom:     float32(req.PriceFrom),
		PriceTo:       float32(req.PriceTo),
	})
	if err != nil {
		h.logger.WithError(err).Error("failed to get all books")
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}
	response := models.GetAllBooksResponse{
		Books: make([]*models.Book, 0),
		Count: int64(res.Count),
	}

	for _, book := range res.Books {
		a := parseBookModel(book)
		response.Books = append(response.Books, &a)
	}

	c.JSON(http.StatusOK, res)
}
