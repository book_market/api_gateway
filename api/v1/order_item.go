package v1

import (
	"context"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/book_market/api_gateway/api/models"
	pb "gitlab.com/book_market/api_gateway/genproto/order_service"
)

// @Router /order-items [post]
// @Summary Create a order item
// @Description Create a order item
// @Tags order_item
// @Accept json
// @Produce json
// @Param category body models.CreateOrderItemRequest true "OrderItem"
// @Success 201 {object} models.Order
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) CreateOrderItem(c *gin.Context) {
	var (
		req *models.CreateOrderItemRequest
	)
	err := c.ShouldBindJSON(&req)
	if err != nil {
		h.logger.WithError(err).Error("failed to bind json")
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	result, err := h.grpcClient.OrderItemService().Create(context.Background(), &pb.OrderItem{
		BookId:      req.BookId,
		OrderId:     req.OrderId,
		ProductCode: req.ProductCode,
		Price:       float32(req.Price),
		Count:       req.Count,
	})
	if err != nil {
		h.logger.WithError(err).Error("failed to create order item")
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusCreated, parseOrderItemModel(result))
}

func parseOrderItemModel(order *pb.OrderItem) *models.OrderItem {
	return &models.OrderItem{
		Id:          order.Id,
		BookId:      order.BookId,
		OrderId:     order.OrderId,
		ProductCode: order.ProductCode,
		Price:       float64(order.Price),
		Count:       order.Count,
		TotalPrice:  float64(order.TotalPrice),
		CreatedAt:   order.CreatedAt,
	}
}
