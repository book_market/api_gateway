package api

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	swaggerFiles "github.com/swaggo/files"     // swagger embed files
	ginSwagger "github.com/swaggo/gin-swagger" // gin-swagger middleware
	_ "gitlab.com/book_market/api_gateway/api/docs"
	v1 "gitlab.com/book_market/api_gateway/api/v1"
	"gitlab.com/book_market/api_gateway/config"
	grpcPkg "gitlab.com/book_market/api_gateway/pkg/grpc_client"
)

type RouterOptions struct {
	Cfg        *config.Config
	GrpcClient grpcPkg.GrpcClientI
	Logger     *logrus.Logger
}

// @title           Swagger for book market api
// @version         1.0
// @description     This is a book market service api.
// @BasePath  /v1
// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
// @Security ApiKeyAuth
func New(opt *RouterOptions) *gin.Engine {
	router := gin.Default()

	handlerV1 := v1.New(&v1.HandlerV1Options{
		Cfg:        opt.Cfg,
		GrpcClient: opt.GrpcClient,
		Logger:     opt.Logger,
	})

	apiV1 := router.Group("/v1")

	apiV1.POST("/auth/register", handlerV1.Register)
	apiV1.POST("/auth/verify", handlerV1.Verify)
	apiV1.POST("/auth/login", handlerV1.Login)
	apiV1.POST("/auth/verify-forgot-password", handlerV1.VerifyRorgotPassword)
	apiV1.POST("/auth/forgot-password", handlerV1.ForgotPassword)

	apiV1.POST("/users", handlerV1.AuthMiddleware("users", "create"), handlerV1.CreateUser)
	apiV1.PUT("/users/:id", handlerV1.AuthMiddleware("users", "update"), handlerV1.UpdateUser)
	apiV1.GET("/users/:id", handlerV1.GetUser)
	apiV1.GET("/users/email/:email", handlerV1.GetUserByEmail)
	apiV1.GET("/users", handlerV1.GetAllUsers)
	apiV1.DELETE("/users/:id", handlerV1.AuthMiddleware("users", "delete"), handlerV1.DeleteUser)

	apiV1.POST("/books", handlerV1.AuthMiddleware("books", "create"), handlerV1.CreateBook)
	apiV1.GET("/books/:id", handlerV1.GetBook)
	apiV1.DELETE("/books/:id", handlerV1.AuthMiddleware("books", "delete"), handlerV1.DeleteBook)
	apiV1.GET("/books", handlerV1.GetAllBooks)

	apiV1.POST("/authors", handlerV1.AuthMiddleware("authors", "create"), handlerV1.CreateAuthor)
	apiV1.GET("/authors/:id", handlerV1.GetAuthor)
	apiV1.DELETE("/authors/:id", handlerV1.AuthMiddleware("authors", "delete"), handlerV1.DeleteAuthor)
	apiV1.GET("/authors", handlerV1.GetAllAuthors)

	apiV1.POST("/categories", handlerV1.AuthMiddleware("categories", "create"), handlerV1.CreateCategory)
	apiV1.GET("/categories/:id", handlerV1.GetCategory)
	apiV1.DELETE("/categories/:id", handlerV1.AuthMiddleware("categories", "delete"), handlerV1.DeleteCategory)
	apiV1.GET("/categories", handlerV1.GetAllCategories)

	apiV1.POST("/reviews", handlerV1.AuthMiddleware("reviews", "create"), handlerV1.CreateReview)
	apiV1.GET("/reviews/:id", handlerV1.GetReview)
	apiV1.DELETE("/reviews/:id", handlerV1.AuthMiddleware("reviews", "delete"), handlerV1.DeleteReview)
	apiV1.GET("/reviews", handlerV1.GetAllReviews)

	apiV1.POST("/orders", handlerV1.CreateOrder)
	apiV1.GET("/orders/:id", handlerV1.GetOrder)
	apiV1.DELETE("/orders/:id", handlerV1.AuthMiddleware("orders", "delete"), handlerV1.DeleteOrder)
	apiV1.GET("/orders", handlerV1.GetAllOrders)
	apiV1.PUT("/orders/:id", handlerV1.AuthMiddleware("orders", "update"), handlerV1.UpdateStatus)

	apiV1.POST("/order-items", handlerV1.CreateOrderItem)

	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	return router

}