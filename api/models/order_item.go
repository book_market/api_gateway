package models

type OrderItem struct {
	Id          int64   `json:"id"`
	BookId      int64   `json:"book_id"`
	OrderId     int64   `json:"order_id"`
	ProductCode int64   `json:"product_code"`
	Price       float64 `json:"price"`
	Count       int64   `json:"count"`
	TotalPrice  float64 `json:"total_price"`
	CreatedAt   string  `json:"created_at"`
}

type CreateOrderItemRequest struct {
	BookId      int64   `json:"book_id"`
	OrderId     int64   `json:"order_id"`
	ProductCode int64   `json:"product_code"`
	Price       float64 `json:"price"`
	Count       int64   `json:"count"`
}

type GetAllOrderItemsResponse struct {
	OrderItems []*OrderItem `json:"order_items"`
	Count  int32    `json:"count"`
}

