package models

type Author struct {
	Id        int64  `json:"id"`
	Fullname  string `json:"fullname"`
	CreatedAt string `json:"created_at"`
}

type GetAllAuthorsResponse struct {
	Authors []*Author `json:"author"`
	Count   int64     `json:"count"`
}

type CreateAuthorRequest struct {
	Fullname string `json:"fullname"`
}
