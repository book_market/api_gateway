package models

type Category struct {
	Id        int64  `json:"id"`
	Name      string `json:"name"`
	ImageUrl  string `json:"image_url"`
	CreatedAt string `json:"created_at"`
}

type GetAllCategoriesResponse struct {
	Categories []*Category `json:"categories"`
	Count      int64       `json:"count"`
}

type CreateCategoryRequest struct {
	Name     string `json:"name"`
	ImageUrl string `json:"image_url"`
}
