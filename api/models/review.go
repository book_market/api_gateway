package models

type Review struct {
	Id         int64       `json:"id"`
	BookId     int64       `json:"book_id"`
	BookData   GetBookInfo `json:"book_info"`
	CustomerId int64       `json:"customer_id"`
	Review     string      `json:"review"`
	CreatedAt  string      `json:"created_at"`
}

type GetAllReviewsResponse struct {
	Reviews []*Review `json:"review"`
	Count   int64     `json:"count"`
}

type CreateReviewRequest struct {
	BookId int64  `json:"book_id"`
	CustomerId int64  `json:"customer_id"`
	Review string `json:"review"`
}

type GetBookInfo struct {
	Name          string  `json:"name"`
	CategoryId    int64   `json:"category_id"`
	AuthorId      int64   `json:"author_id"`
	Description   string  `json:"description"`
	PublisherName string  `json:"publisher_name"`
	Language      string  `json:"language"`
	Inscription   string  `json:"inscription"`
	Page          int64   `json:"page"`
	Isbn          string  `json:"isbn"`
	Format        string  `json:"format"`
	Wrapper       string  `json:"wrapper"`
	ImageUrl      string  `json:"image_url"`
	Price         float64 `json:"price"`
	CreatedAt     string  `json:"created_at"`
}
