package models

type Order struct {
	Id            int64   `json:"id"`
	UserId        int64   `json:"user_id"`
	OrderNumber   int64   `json:"order_number"`
	TotalAmount   float64 `json:"total_amount"`
	Region        string  `json:"region"`
	District      string  `json:"district"`
	Address       string  `json:"address"`
	Status        string  `json:"status"`
	PaymentStatus string  `json:"payment_status"`
	CreatedAt     string  `json:"created_at"`
}

type GetAllOrdersResponse struct {
	Orders []*Order `json:"orders"`
	Count  int32    `json:"count"`
}

type CreateOrderRequest struct {
	UserId        int64   `json:"user_id"`
	OrderNumber   int64   `json:"order_number"`
	TotalAmount   float64 `json:"total_amount"`
	Region        string  `json:"region"`
	District      string  `json:"district"`
	Address       string  `json:"address"`
	Status        string  `json:"status"`
	PaymentStatus string  `json:"payment_status"`
}

type UpdateStatus struct {
	Status string `json:"status"`
}
