package models

type User struct {
	Id          int64  `json:"id"`
	FirstName   string `json:"first_name"`
	Lastname    string `json:"last_name"`
	Email       string `json:"email"`
	Password    string `json:"password"`
	PhoneNumber string `json:"phone_number"`
	ImageUrl    string `json:"image_url"`
	Type        string `json:"type"`
	CreatedAt   string `json:"created_at"`
}

type CreateUserRequest struct {
	FirstName   string `json:"first_name" binding:"required,min=2,max=30"`
	Lastname    string `json:"last_name" binding:"required,min=2,max=30"`
	Email       string `json:"email" binding:"required,email"`
	Password    string `json:"password" binding:"required,min=6,max=16"`
	PhoneNumber string `json:"phone_number"`
	ImageUrl    string `json:"image_url"`
	Type        string `json:"type" binding:"required,oneof=superadmin user"`
}

type UpdateUserRequest struct {
	FirstName   string `json:"first_name" binding:"required,min=2,max=30"`
	Lastname    string `json:"last_name" binding:"required,min=2,max=30"`
	PhoneNumber string `json:"phone_number"`
	ImageUrl    string `json:"image_url"`
}

type GetAllUsersResponse struct {
	Users []*User `json:"users"`
	Count int32   `json:"count"`
}
