package models

type Book struct {
	Id            int64           `json:"id"`
	Name          string          `json:"name"`
	CategoryId    int64           `json:"category_id"`
	CategoryData  GetCategoryInfo `json:"category_info"`
	AuthorId      int64           `json:"author_id"`
	AuthorData    GetAuthorInfo   `json:"author_info"`
	Description   string          `json:"description"`
	PublisherName string          `json:"publisher_name"`
	Language      string          `json:"language"`
	Inscription   string          `json:"inscription"`
	Page          int64           `json:"page"`
	Isbn          string          `json:"isbn"`
	Format        string          `json:"format"`
	Wrapper       string          `json:"wrapper"`
	ImageUrl      string          `json:"image_url"`
	Price         float64         `json:"price"`
	CreatedAt     string          `json:"created_at"`
}

type GetAllBooksResponse struct {
	Books []*Book
	Count int64
}

type GetAllBooksParams struct {
	Limit         int32   `json:"limit" binding:"required" default:"10"`
	Page          int32   `json:"page" binding:"required" default:"1"`
	Name          string  `json:"name"`
	AuthorId      int64   `json:"author_id"`
	CategoryId    int64   `json:"category_id"`
	PublisherName string  `json:"publisher_name"`
	Language      string  `json:"language"`
	Inscription   string  `json:"inscription"`
	Wrapper       string  `json:"wrapper"`
	PriceFrom     float64 `json:"price_from"`
	PriceTo       float64 `json:"price_to"`
}

type CreateBookRequest struct {
	Name          string  `json:"name"`
	CategoryId    int64   `json:"category_id"`
	AuthorId      int64   `json:"author_id"`
	Description   string  `json:"description"`
	PublisherName string  `json:"publisher_name"`
	Language      string  `json:"language"`
	Inscription   string  `json:"inscription"`
	Page          int64   `json:"page"`
	Isbn          string  `json:"isbn"`
	Format        string  `json:"format"`
	Wrapper       string  `json:"wrapper"`
	ImageUrl      string  `json:"image_url"`
	Price         float64 `json:"price"`
}

type GetCategoryInfo struct {
	Name      string `json:"name"`
	ImageUrl  string `json:"image_url"`
	CreatedAt string `json:"created_at"`
}

type GetAuthorInfo struct {
	Fullname  string `json:"fullname"`
	CreatedAt string `json:"created_at"`
}
