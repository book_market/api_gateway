package grpc_client

import (
	"fmt"

	"gitlab.com/book_market/api_gateway/config"
	pb "gitlab.com/book_market/api_gateway/genproto/book_service"
	pbo "gitlab.com/book_market/api_gateway/genproto/order_service"
	pbu "gitlab.com/book_market/api_gateway/genproto/user_service"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type GrpcClientI interface {
	UserService() pbu.UserServiceClient
	AuthService() pbu.AuthServiceClient
	BookService() pb.BookServiceClient
	AuthorService() pb.AuthorServiceClient
	CategoryService() pb.CategoryServiceClient
	ReviewService() pb.ReviewServiceClient
	OrderService() pbo.OrderServiceClient
	OrderItemService() pbo.OrderItemServiceClient
}

type GrpcClient struct {
	cfg         config.Config
	connections map[string]interface{}
}

func New(cfg config.Config) (GrpcClientI, error) {
	connUserService, err := grpc.Dial(
		fmt.Sprintf("%s%s", cfg.UserServiceHost, cfg.UserServiceGrpcPort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, fmt.Errorf("user service dial host: %v port: %v", cfg.UserServiceHost, cfg.UserServiceGrpcPort)
	}

	connBookService, err := grpc.Dial(
		fmt.Sprintf("%s%s", cfg.BookServiceHost, cfg.BookServiceGrpcPort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, fmt.Errorf("order service dial host: %v port: %v", cfg.BookServiceHost, cfg.BookServiceGrpcPort)
	}

	connOrderService, err := grpc.Dial(
		fmt.Sprintf("%s%s", cfg.OrderServiceHost, cfg.OrderServiceGrpcPort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, fmt.Errorf("book service dial host: %v port: %v", cfg.OrderServiceHost, cfg.OrderServiceGrpcPort)
	}

	return &GrpcClient{
		cfg: cfg,
		connections: map[string]interface{}{
			"user_service":       pbu.NewUserServiceClient(connUserService),
			"auth_service":       pbu.NewAuthServiceClient(connUserService),
			"book_service":       pb.NewBookServiceClient(connBookService),
			"author_service":     pb.NewAuthorServiceClient(connBookService),
			"category_service":   pb.NewCategoryServiceClient(connBookService),
			"review_service":     pb.NewReviewServiceClient(connBookService),
			"order_service":      pbo.NewOrderServiceClient(connOrderService),
			"order_item_service": pbo.NewOrderItemServiceClient(connOrderService),
		},
	}, nil
}

func (g *GrpcClient) UserService() pbu.UserServiceClient {
	return g.connections["user_service"].(pbu.UserServiceClient)
}

func (g *GrpcClient) AuthService() pbu.AuthServiceClient {
	return g.connections["auth_service"].(pbu.AuthServiceClient)
}

func (g *GrpcClient) BookService() pb.BookServiceClient {
	return g.connections["book_service"].(pb.BookServiceClient)
}

func (g *GrpcClient) AuthorService() pb.AuthorServiceClient {
	return g.connections["author_service"].(pb.AuthorServiceClient)
}

func (g *GrpcClient) CategoryService() pb.CategoryServiceClient {
	return g.connections["category_service"].(pb.CategoryServiceClient)
}

func (g *GrpcClient) ReviewService() pb.ReviewServiceClient {
	return g.connections["review_service"].(pb.ReviewServiceClient)
}

func (g *GrpcClient) OrderService() pbo.OrderServiceClient {
	return g.connections["order_service"].(pbo.OrderServiceClient)
}

func (g *GrpcClient) OrderItemService() pbo.OrderItemServiceClient {
	return g.connections["order_item_service"].(pbo.OrderItemServiceClient)
}
